#include <iostream>

#include "LaserController.h"

int main() {
    auto laser = Laser::Controller();
    while(true){
        std::string command;
        getline(std::cin, command);
        std::cout << laser.onCommand(command);
    }
}