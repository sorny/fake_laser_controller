//
// Created by esteve on 02/05/2021.
//

#ifndef FAKE_LASER_CONTROLLER_LASERCONTROLLER_H
#define FAKE_LASER_CONTROLLER_LASERCONTROLLER_H


#include <string>
#include <chrono>
#include <vector>
#include <thread>

namespace Laser {
    enum EmissionStatus {
        OFF = 0, ON = 1
    };

    class Controller {
    public:
        Controller();
        ~Controller();
        std::string onCommand(const std::string &command);

    private:
        static std::string ackResponse(const std::string &command);

        static std::string errorResponse(const std::string &command);

        static std::string unknownCommandError();

        template<typename T>
        std::string valueResponse(const std::string &command, const std::vector<T> &values) const;

        template<typename T>
        std::string valueResponse(const std::string &command, T value) const;

        void keep_alive_check();

        EmissionStatus emissionStatus{OFF};

        std::chrono::time_point<std::chrono::system_clock> last_keep_alive;

        std::thread keep_alive_checker;
        bool running_checker{true};

        int laser_power{0};
        bool silly_mode{false};
    };
}


#endif //FAKE_LASER_CONTROLLER_LASERCONTROLLER_H
