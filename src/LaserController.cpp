//
// Created by esteve on 02/05/2021.
//

#include <sstream>
#include <thread>
#include <algorithm>
#include <iostream>
#include "LaserController.h"

using std::stringstream;


Laser::Controller::Controller() {
    last_keep_alive = std::chrono::system_clock::now();
    keep_alive_checker = std::thread(&Controller::keep_alive_check, this);
}

// TODO: All this if the project start to grow would be more useful if it was converted to a states pattern to create a states machine.
std::string Laser::Controller::onCommand(const std::string &raw_command) {
    auto command = raw_command;
    if (silly_mode) {
        std::reverse(command.begin(), command.end());
    }
    if (command == "STR") {
        if (emissionStatus == OFF) {
            emissionStatus = ON;
            last_keep_alive = std::chrono::system_clock::now();
            return ackResponse(command);
        }
        return errorResponse(command);
    }
    if (command == "STP") {
        if (emissionStatus == ON) {
            emissionStatus = OFF;
            return ackResponse(command);
        }
        return errorResponse(command);
    }
    if (command == "ST?") {
        return valueResponse<int>(command, emissionStatus);
    }
    if (command == "KAL") {
        if (emissionStatus == ON) {
            last_keep_alive = std::chrono::system_clock::now();
        }
        return ackResponse(command);
    }
    if (command == "PW?") {
        int value = emissionStatus == ON ? laser_power : 0;
        return valueResponse<int>(command, value);
    }

    //TODO: This part will need fuzzy testing to be sure it's reliable to all kind of inputs only a few is going to be tested
    if (command.find("PW=|") != std::string::npos) {
        try {
            auto n = command.substr(sizeof("PW=|") - 1);
            laser_power = stoi(n);
            return ackResponse("PW=");
        } catch (const std::exception &e) {
            return errorResponse(command);
        }
    }

    if (command == "ESM") {
        silly_mode = true;
        return ackResponse(command);
    }

    if (command == "DSM") {
        silly_mode = false;
        return ackResponse(command);
    }
    return unknownCommandError();
}

std::string Laser::Controller::ackResponse(const std::string &command) {
    return command + "#\n";
}

std::string Laser::Controller::errorResponse(const std::string &command) {
    return command + "!\n";
}

std::string Laser::Controller::unknownCommandError() {
    return "UK!\n";
}

template<typename T>
std::string Laser::Controller::valueResponse(const std::string &command, const std::vector<T> &values) const {
    stringstream ss;
    ss << command;

    for (const auto &v: values) {
        ss << '|' << v;
    }
    ss << "#\n";
    return ss.str();
}

template<typename T>
std::string Laser::Controller::valueResponse(const std::string &command, T value) const {
    return valueResponse<T>(command, std::vector<T>({value}));
}


void Laser::Controller::keep_alive_check() {
    using namespace std::this_thread;     // sleep_for, sleep_until
    using namespace std::chrono_literals; // ns, us, ms, s, h, etc.
    while (running_checker) {
        if (emissionStatus == OFF) {
            sleep_for(5s);
            continue;
        }
        auto now = std::chrono::system_clock::now();
        sleep_for(5s - (now - last_keep_alive));
        now = std::chrono::system_clock::now();
        if ((now - last_keep_alive) >= 5s) {
            emissionStatus = OFF;
        }
    }
}

Laser::Controller::~Controller() {
    running_checker = false;
    keep_alive_checker.join();
}
