//
// Created by esteve on 02/05/2021.
//

#include <catch2/catch.hpp>

#include <chrono>
#include <thread>

#include "LaserController.h"


TEST_CASE("Test command without return values") {
    auto laser = Laser::Controller();
    auto response = laser.onCommand("STR");
    REQUIRE(response == "STR#\n");

}

TEST_CASE("Test command with error") {
    auto laser = Laser::Controller();
    laser.onCommand("STR");
    auto response = laser.onCommand("STR");
    REQUIRE(response == "STR!\n");
}

TEST_CASE("Test command with value response") {
    auto laser = Laser::Controller();
    auto response = laser.onCommand("ST?");
    REQUIRE(response == "ST?|0#\n");
}

TEST_CASE("Test response unknown commnand") {
    auto laser = Laser::Controller();
    auto response = laser.onCommand("ASHFDSK");
    REQUIRE(response == "UK!\n");

}

TEST_CASE("Test keep alive. It should be emitting once receive the command STR and after 3 seconds send ST? and should return 1. 6 seconds later should be a 0") {
    using namespace std::this_thread;     // sleep_for, sleep_until
    using namespace std::chrono_literals; // ns, us, ms, s, h, etc.
    using std::chrono::system_clock;


    auto laser = Laser::Controller();
    auto response = laser.onCommand("STR");
    REQUIRE(response == "STR#\n");

    response = laser.onCommand("ST?");
    REQUIRE(response == "ST?|1#\n");

    sleep_for(3s);
    response = laser.onCommand("ST?");
    REQUIRE(response == "ST?|1#\n");

    response = laser.onCommand("KAL");
    REQUIRE(response == "KAL#\n");

    sleep_for(2s);
    response = laser.onCommand("ST?");
    REQUIRE(response == "ST?|1#\n");

    sleep_for(4s);
    response = laser.onCommand("ST?");
    REQUIRE(response == "ST?|0#\n");
}

TEST_CASE("Test power return. Should be 0 if not emitting") {
    auto laser = Laser::Controller();
    auto response = laser.onCommand("PW?");
    REQUIRE(response == "PW?|0#\n");
}

TEST_CASE("Test power return when emitting") {
    auto laser = Laser::Controller();
    auto response = laser.onCommand("STR");
    REQUIRE(response == "STR#\n");

    response = laser.onCommand("PW=|100");
    REQUIRE(response == "PW=#\n");

    response = laser.onCommand("PW?");
    REQUIRE(response == "PW?|100#\n");
}

TEST_CASE("Test power return should fail because the laser will not be emitting") {
    auto laser = Laser::Controller();
    auto response = laser.onCommand("PW?");
    REQUIRE(response == "PW?|0#\n");
}

TEST_CASE("Test laser power setter. Correct setting") {
    auto laser = Laser::Controller();
    auto response = laser.onCommand("STR");
    REQUIRE(response == "STR#\n");

    response = laser.onCommand("PW=|50");
    REQUIRE(response == "PW=#\n");

    response = laser.onCommand("PW?");
    REQUIRE(response == "PW?|50#\n");
}

TEST_CASE("Test laser power setter. wrong parameter") {
    auto laser = Laser::Controller();
    auto response = laser.onCommand("STR");
    REQUIRE(response == "STR#\n");

    response = laser.onCommand("PW=|dsosdgdgcv");
    REQUIRE(response == "PW=|dsosdgdgcv!\n");
}

TEST_CASE("Test silly mode") {
    auto laser = Laser::Controller();
    auto response = laser.onCommand("ESM");
    REQUIRE(response == "ESM#\n");

    response = laser.onCommand("RTS");
    REQUIRE(response == "STR#\n");

    response = laser.onCommand("?TS");
    REQUIRE(response == "ST?|1#\n");

    response = laser.onCommand("MSD");
    REQUIRE(response == "DSM#\n");

    response = laser.onCommand("ST?");
    REQUIRE(response == "ST?|1#\n");
}